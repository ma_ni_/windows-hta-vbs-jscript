﻿function changeBGColor(color){
  document.body.style.backgroundColor =color
}

function changeColorTagHeader(color){
  var h1array = document.getElementsByTagName("h1")
  for (var i = 0; i < h1array.length; i ++) {
  	h1array[i].style.color = color;
  }
  var h2array = document.getElementsByTagName("h2")
  for (var i = 0; i < h2array.length; i ++) {
  	h2array[i].style.color = color;
  }
  var h3array = document.getElementsByTagName("h3")
  for (var i = 0; i < h3array.length; i ++) {
  	h3array[i].style.color = color;
  }
  var h4array = document.getElementsByTagName("h4")
  for (var i = 0; i < h4array.length; i ++) {
  	h4array[i].style.color = color;
  }
  var h5array = document.getElementsByTagName("h5")
  for (var i = 0; i < h5array.length; i ++) {
  	h5array[i].style.color = color;
  }
  var h6array = document.getElementsByTagName("h6")
  for (var i = 0; i < h6array.length; i ++) {
  	h6array[i].style.color = color;
  }
  var dtarray = document.getElementsByTagName("dt")
  for (var i = 0; i < dtarray.length; i ++) {
  	dtarray[i].style.color = color;
  }
}

function changeColorTagItalic(color){
  var emarray = document.getElementsByTagName("em")
  for (var i = 0; i < emarray.length; i ++) {
  	emarray[i].style.color = color;
  }
  var prearray = document.getElementsByTagName("pre")
  for (var i = 0; i < prearray.length; i ++) {
  	prearray[i].style.color = color;
  }
  var codearray = document.getElementsByTagName("code")
  for (var i = 0; i < codearray.length; i ++) {
  	codearray[i].style.color = color;
  }
}

function innerHTMLFile(text){
  replace.innerHTML = text
}
