    ' 初期表示時イベント
    Sub Window_Onload  

        Set objWshShell = CreateObject("WScript.Shell")
        
        If Err.Number = 0 Then
            ListFiles(objWshShell.CurrentDirectory)
        Else
            ListFiles("C:")
        End If

        Set objWshShell = Nothing
  
    End Sub  

    ' ファイル一覧表示
    Sub ListFiles(ByVal dirName)
        strComputer = "."
        
        Set objWMIService = GetObject("winmgmts:\\" & strComputer & "\root\cimv2")  
  
        Set colFiles = objWMIService.ExecQuery _  
            ("ASSOCIATORS OF {Win32_Directory.Name='" & dirName & "'} Where " _  
                & "ResultClass = CIM_DataFile")  

        DirectoryNameBox.value = dirName

        For Each objFile In colFiles  
            Set objOption = Document.createElement("OPTION")  
            objOption.Text = objFile.FileName  
            objOption.Value = objFile.Name  
            LogFiles.Add(objOption)  
        Next  

    End Sub

    ' ファイル選択ダイアログ表示
    Sub SelectDirectory
        Dim objShell    ' Shell オブジェクト
        Dim objFolder   ' フォルダ情報

        Set objShell = CreateObject("Shell.Application")

        If Err.Number = 0 Then
            Set objFolder = objShell.BrowseForFolder(0, "ファイルを一覧表示するディレクトリを選択してください", 0)
            If Not objFolder Is Nothing Then
                DirectoryNameBox.value = objFolder.Items.Item.Path
                ListFiles(objFolder.Items.Item.Path)
            End If

        Else
            alert("エラー：" & Err.Description)
        End If
    End Sub
  
    ' ファイル情報の表示
    Sub ReadFile  
        Set objFSO = CreateObject("Scripting.FileSystemObject")  
        Set objFile = objFSO.OpenTextFile(LogFiles.Value)  
  
        strContents = objFile.ReadAll  
  
        objFile.Close  
  
        BasicTextArea.Value = strContents  
        innerHTMLFile(strContents)
        'FileNameBox.Value = LogFiles.Value
        FileNameBox.Value = Replace(LogFiles.Value, LCase(DirectoryNameBox.value & "\"), "")

    End Sub  
      
    ' ファイルへのデータ保存
    Sub SaveBGColor(ByVal color)  
        Set objFSO = CreateObject("Scripting.FileSystemObject")  
        
        Set objAllFile =  objFSO.OpenTextFile("change_from_hta.css", 1)
        allString = objAllFile.ReadAll()
        objAllFile.Close 

        Set objLineFile =  objFSO.OpenTextFile("change_from_hta.css", 1) 
        objLineFile.SkipLine()
        lineString = objLineFile.ReadLine()
        objLineFile.Close 

        Set objFile = objFSO.OpenTextFile("change_from_hta.css", 2)  
  
        replaceString = Replace(allString, lineString, "background-color:" + color + ";")
        objFile.WriteLine (replaceString)  
        objFile.Close  
    End Sub 