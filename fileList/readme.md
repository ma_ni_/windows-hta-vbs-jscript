# HTA調査

---

### 目標 
* HTAでできることの調査
* .Netとの違いを把握する

### 現状
* ファイルアクセスはWSHが使えて、比較的簡単にできた
* HTMLやJavaScriptやCSSは問題なく使えそう  
　  
  
---  
  
![img1](https://bitbucket.org/ma_ni/windows-hta-vbs-jscript/raw/a0ec3f0d534e492b915a6002eaef8d1a658dc550/fileList/md_files/ss.png)

> #### 今の機能
> * 選択ディレクトリにあるファイルをリスト表示し、それが選択された場合に、内容を表示する
> * 読み込み時にエラーが発生する場合あり（エラーハンドリングはまだしていない）

> #### 今後やりたい機能
> * 勤怠管理をみるアプリの作成（カレンダー選択ライブラリがJavaScriptにあるので、それを利用して、日付選択　→　innerHTMLでの表示）
> * HTMLビルダー的なものを作ったほうがいいか検討する（Visua StudioやEditorで作ったほうが早いかどうか調査）   

---

最終更新 2013年11月05日 (火) 0:50