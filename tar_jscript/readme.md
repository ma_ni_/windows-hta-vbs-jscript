# COM調査

---

### 目標 
* .NetとVBScriptとの連携
* DLLと連携するためにCOMを理解する

### 現状
* VBScriptでDLLを使用する場合、rundll32.exeを用いれば簡単に利用できる
* DLLの内容はdumpbin（Visual Studioにあるdll）を利用すればAPIがわかる
* .Netで作成したクラスライブラリを上記のDLLと同じような形にすることができるまで調査する
　  
  
---  
  
![img1](https://bitbucket.org/ma_ni/windows-hta-vbs-jscript/raw/c9f773ea496f32b4593e187357535fb21757a13e/tar_jscript/md_files/ss.png)

> #### 今の機能
> * tar32.dllを用いて、test.tarを解答する

> #### 今後やりたい機能
> * .netで作成したクラスライブラリを利用できるようにする

---

最終更新 2013年11月16日 (土) 19:06