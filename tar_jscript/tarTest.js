var objWshShell;
var objExecCmd;

try
{
    objWshShell = WScript.CreateObject("WScript.Shell");
    objWshShell.Run("rundll32.exe tar32.dll TarCommandLine xvf test.tar")
} catch (e)
{
    WScript.ECho(e.description);
} finally
{
    objWshShell = null;
    objExecCmd = null;
}
